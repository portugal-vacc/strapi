/**
 * aircraft-type controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::aircraft-type.aircraft-type');
