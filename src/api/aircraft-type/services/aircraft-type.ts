/**
 * aircraft-type service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::aircraft-type.aircraft-type');
